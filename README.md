# Guardian software test assignment

Simple RESTfull API that exposes CRUD operations for quiz application's questions.

## Deployment

Prerequisites:

- docker
- docker compose

1. Open the terminal, go to the root directory of the project and run the command below to build executable jar

```bash
./gradlew clean build -x test
```

2. After building the jar run the next two commands bellow to start the application inside a docker container

```bash
docker compose build && docker compose up -d
```

After the application is successfully started and running, it will listen to port 8080. So, make sure the port is not occupied.

In order to stop the application, run:

```bash
docker compose down
```

## API

### 1. Create a question

**POST /question**

Request body:

```json
{
  "question":"Elton John is originally from?",
  "options": [
    {
      "option": "England",
      "correct": true
    },
    {
      "option": "Spain",
      "correct": false
    }
  ]
}
```

Response body:

```json
{
    "id": 1,
    "question": "Elton John is originally from?",
    "options": [
        {
            "option": "England",
            "correct": true
        },
        {
            "option": "Spain",
            "correct": false
        }
    ]
}
```

### 2. Update a question

**PUT /question/{id}**

Request body:

```json
{
    "question": "When was the \"The Beatles\" music band formed?",
    "options": [
        {
            "option": "In 1970's",
            "correct": false
        },
        {
            "option": "In 1960's",
            "correct": true
        }
    ]
}
```

Response body:

```json
{
    "id": 1,
    "question": "When was the \"The Beatles\" music band formed?",
    "options": [
        {
            "option": "In 1970's",
            "correct": false
        },
        {
            "option": "In 1960's",
            "correct": true
        }
    ]
}
```
### 3. Fetch all questions (paged)

**GET /question**

Request parameters:
- questionPart (optional) - Question substring/part. Questions that do not contain given substring are filtered out
- page (optional) - page number, starts from zero
- size (optional) - page size

Response body:

```json
{
    "content": [
        {
            "id": 2,
            "question": "Elton John is originally from?",
            "options": [
                {
                    "option": "England",
                    "correct": true
                },
                {
                    "option": "Spain",
                    "correct": false
                }
            ]
        },
        {
            "id": 1,
            "question": "When was the \"The Beatles\" music band formed?",
            "options": [
                {
                    "option": "In 1970's",
                    "correct": false
                },
                {
                    "option": "In 1960's",
                    "correct": true
                }
            ]
        }
    ],
    "pageable": {
        "sort": {
            "unsorted": false,
            "sorted": true,
            "empty": false
        },
        "pageNumber": 0,
        "pageSize": 20,
        "offset": 0,
        "paged": true,
        "unpaged": false
    },
    "totalPages": 1,
    "totalElements": 2,
    "last": true,
    "numberOfElements": 2,
    "size": 20,
    "number": 0,
    "sort": {
        "unsorted": false,
        "sorted": true,
        "empty": false
    },
    "first": true,
    "empty": false
}
```

### 4. Fetch specific question

**GET /question/{id}**

Response body:

```json
{
    "id": 1,
    "question": "When was the \"The Beatles\" music band formed?",
    "options": [
        {
            "option": "In 1970's",
            "correct": false
        },
        {
            "option": "In 1960's",
            "correct": true
        }
    ]
}
```

### 5. Delete question

**DELETE /question/{id}**

Response body: Always returns OK (200)
