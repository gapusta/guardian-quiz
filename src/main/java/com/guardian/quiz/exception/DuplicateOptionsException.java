package com.guardian.quiz.exception;

public class DuplicateOptionsException extends IllegalArgumentException {
    public DuplicateOptionsException() {
        super("Duplicate options");
    }
}
