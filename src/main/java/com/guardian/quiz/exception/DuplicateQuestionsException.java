package com.guardian.quiz.exception;

public class DuplicateQuestionsException extends IllegalArgumentException {
    public DuplicateQuestionsException() {
        super("Question already exists");
    }
}
