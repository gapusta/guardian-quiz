package com.guardian.quiz.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Option {
    private String option;
    private Boolean correct;
}
