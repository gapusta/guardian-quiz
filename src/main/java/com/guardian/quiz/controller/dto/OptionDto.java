package com.guardian.quiz.controller.dto;

import com.guardian.quiz.model.Option;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OptionDto {
    private String option;
    private Boolean correct;

    public OptionDto(Option option) {
        this.option = option.getOption();
        this.correct = option.getCorrect();
    }
}
