package com.guardian.quiz.controller.dto;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OptionRequest {

    @NotBlank
    private String option;

    private Boolean correct = false;

    public void setCorrect(Boolean correct) {
        this.correct = correct != null ? correct : false;
    }

}
