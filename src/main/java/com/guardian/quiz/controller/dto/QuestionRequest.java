package com.guardian.quiz.controller.dto;

import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class QuestionRequest {

    @NotBlank
    private String question;

    @NotEmpty
    private List<@Valid OptionRequest> options;

}
