package com.guardian.quiz.controller.dto;

import com.guardian.quiz.model.Question;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class QuestionDto {
    private Long id;
    private String question;
    private List<OptionDto> options;

    public QuestionDto(Question question) {
        this.id = question.getId();
        this.question = question.getQuestion();
        this.options = question.getOptions().stream().map(OptionDto::new).toList();
    }
}
