package com.guardian.quiz.controller.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuestionSearch {
    private String questionPart;
}
