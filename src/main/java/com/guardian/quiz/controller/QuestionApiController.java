package com.guardian.quiz.controller;

import com.guardian.quiz.controller.dto.QuestionDto;
import com.guardian.quiz.controller.dto.QuestionRequest;
import com.guardian.quiz.controller.dto.QuestionSearch;
import com.guardian.quiz.service.QuestionService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/question")
@RequiredArgsConstructor
public class QuestionApiController {

    private final QuestionService questionService;

    @PostMapping
    public QuestionDto create(@RequestBody @Valid QuestionRequest request) {
        var question = questionService.create(request);
        return new QuestionDto(question);
    }

    @PutMapping("/{id:\\d+}")
    public QuestionDto update(@PathVariable Long id, @RequestBody @Valid QuestionRequest request) {
        var question = questionService.update(id, request);
        return new QuestionDto(question);
    }

    @GetMapping
    public Page<QuestionDto> getAll(
        QuestionSearch search,
        @SortDefault(sort = "id", direction = Sort.Direction.DESC) Pageable pageable
    ) {
        var questions = questionService.getAll(search, pageable);
        return questions.map(QuestionDto::new);
    }

    @GetMapping("/{id}")
    public QuestionDto getById(@PathVariable Long id) {
        var question = questionService.getById(id);
        return new QuestionDto(question);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        questionService.delete(id);
    }

}
