package com.guardian.quiz.controller;

import jakarta.servlet.http.HttpServletRequest;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.util.List;

@ControllerAdvice
public class ErrorHandlingControllerAdvice {

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handle(IllegalArgumentException ex, HttpServletRequest request) {
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), request.getServletPath(), ex.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResponse handle(MethodArgumentNotValidException ex, HttpServletRequest request) {
        var errors =  ex.getBindingResult().getFieldErrors().stream()
                .map(error -> new ValidationError(error.getField(), error.getDefaultMessage()))
                .toList();
        return new ErrorResponse(HttpStatus.BAD_REQUEST.value(), request.getServletPath(), errors);
    }

    @Getter
    @RequiredArgsConstructor
    public static class ErrorResponse {
        private final Instant timestamp = Instant.now();
        private final Integer status;
        private final String path;
        private final Object error;
    }

    public record ValidationError(String fieldName, String message) {}

}
