package com.guardian.quiz.repository;

import com.guardian.quiz.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface QuestionRepository extends JpaRepository<Question, Long> {

    @Query("SELECT q FROM Question q WHERE :part IS NULL OR q.question LIKE concat('%', :part, '%')")
    Page<Question> findByQuestionContaining(@Param("part") String part, Pageable pageable);

    boolean existsByQuestionEquals(String question);

    boolean existsByIdNotAndQuestionEquals(Long id, String question);

}
