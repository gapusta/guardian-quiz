package com.guardian.quiz.service;

import com.guardian.quiz.controller.dto.OptionRequest;
import com.guardian.quiz.controller.dto.QuestionRequest;
import com.guardian.quiz.controller.dto.QuestionSearch;
import com.guardian.quiz.exception.DuplicateOptionsException;
import com.guardian.quiz.exception.DuplicateQuestionsException;
import com.guardian.quiz.exception.NotFoundException;
import com.guardian.quiz.model.Option;
import com.guardian.quiz.model.Question;
import com.guardian.quiz.repository.QuestionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Function;

@Service
@RequiredArgsConstructor
public class DefaultQuestionService implements QuestionService {

    private final QuestionRepository questionRepository;

    @Transactional
    public Question create(QuestionRequest request) {
        var question = new Question();

        validate(request);

        question.setQuestion(request.getQuestion());
        question.setOptions(request.getOptions().stream().map(toOption()).toList());

        return questionRepository.save(question);
    }

    @Transactional
    public Question update(Long id, QuestionRequest request) {
        var question = getById(id);

        validate(id, request);

        question.setQuestion(request.getQuestion());
        question.setOptions(request.getOptions().stream().map(toOption()).toList());

        return questionRepository.save(question);
    }

    @Transactional(readOnly = true)
    public Question getById(Long id) {
        return questionRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @Transactional(readOnly = true)
    public Page<Question> getAll(QuestionSearch search, Pageable pageable) {
        return questionRepository.findByQuestionContaining(search.getQuestionPart(), pageable);
    }

    @Transactional
    public void delete(Long id) {
        questionRepository.deleteById(id);
    }

    private void validate(QuestionRequest request) {
        var options = request.getOptions();
        var question = request.getQuestion();

        if (questionRepository.existsByQuestionEquals(question)) {
            throw new DuplicateQuestionsException();
        }

        validate(options);
    }

    private void validate(Long excludeId, QuestionRequest request) {
        var options = request.getOptions();
        var question = request.getQuestion();

        if (questionRepository.existsByIdNotAndQuestionEquals(excludeId, question)) {
            throw new DuplicateQuestionsException();
        }

        validate(options);
    }

    private void validate(List<OptionRequest> options) {
        var distinctOptions = options.stream().map(OptionRequest::getOption).distinct().count();
        if (distinctOptions < options.size()) {
            throw new DuplicateOptionsException();
        }
    }

    private Function<OptionRequest, Option> toOption() {
        return request -> {
            var option = new Option();
            option.setOption(request.getOption());
            option.setCorrect(request.getCorrect());
            return option;
        };
    }

}
