package com.guardian.quiz.service;

import com.guardian.quiz.controller.dto.QuestionRequest;
import com.guardian.quiz.controller.dto.QuestionSearch;
import com.guardian.quiz.model.Question;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface QuestionService {

    Question create(QuestionRequest request);

    Question update(Long id, QuestionRequest request);

    Question getById(Long id);

    Page<Question> getAll(QuestionSearch search, Pageable pageable);

    void delete(Long id);

}
