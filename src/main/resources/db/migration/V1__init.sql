CREATE TABLE questions (
    id BIGSERIAL PRIMARY KEY,
    question VARCHAR(1023) NOT NULL,
    options JSONB NOT NULL,
    UNIQUE (question)
);
